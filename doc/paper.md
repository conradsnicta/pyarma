---
title: 'PyArmadillo: an alternative approach to linear algebra in Python'
tags:
    - Python
    - linear algebra
    - scientific computing
    - mathematics
authors:
    - name: Jason Rumengan
      orcid: 0000-0003-1839-5138
      affiliation: "1, 2"
    - name: Terry Yue Zhuo
      orcid: 0000-0002-5760-5188
      affiliation: 3
    - name: Conrad Sanderson
      orcid: 0000-0002-0049-4501
      affiliation: "1, 4"
affiliations:
    - name: Data61/CSIRO, Australia
      index: 1
    - name: Queensland University of Technology, Australia
      index: 2
    - name: University of New South Wales, Australia
      index: 3
    - name: Griffith University, Australia
      index: 4
date: 10 February 2021
bibliography: paper.bib
---

# Summary
PyArmadillo is a streamlined linear algebra library for the Python language,
with an emphasis on ease of use.
It aims to provide a high-level syntax and functionality deliberately similar to Matlab/Octave [@Linge_MatlabOctave_2016],
allowing mathematical operations to be expressed in a familiar and natural manner.
PyArmadillo provides objects for matrices and cubes,
as well as over 200 associated functions for manipulating data stored in the objects.
Integer, floating point and complex numbers are supported.
Various matrix factorisations are provided through integration with LAPACK [@anderson1999lapack],
or one of its high performance drop-in replacements such as Intel MKL [@Intel_MKL] or OpenBLAS [@Xianyi_OpenBLAS].

# Statement of need
While frameworks such as NumPy [@harris2020array] and SciPy [@2020SciPy-NMeth] are available for Python,
they tend to be overly verbose and somewhat cumbersome to use in comparison to Matlab/Octave.
From a linear algebra point of view, 
the issues include: **(i)** a focus on operations involving multi-dimensional arrays rather than matrices,
**(ii)** nested organisation of functions which increases code verbosity (which in turn can lead to reduced user productivity),
and **(iii)** syntax that notably differs from Matlab.
PyArmadillo aims to address these issues by primarily focusing on linear algebra,
having all functions accessible in one flat structure,
and providing an API that closely follows Matlab.

# Implementation
PyArmadillo relies on Armadillo [@Sanderson_2016; @Sanderson_2018] for the underlying C++ implementation of matrix objects and associated functions,
as well as on pybind11 [@pybind11] for interfacing C++ and Python. 
Due to its expressiveness and relatively straightforward use,
pybind11 was selected over other interfacing approaches such as Boost.Python [@osti_815409] and manually writing C++ extensions for Python.
Armadillo was selected due to its API having close similarity to Matlab,
as well as previous success with RcppArmadillo, a widely used bridge between the R language and C++ [@Eddelbuettel_2014].

# Acknowledgements
We would like to thank our colleagues at Data61/CSIRO (Andrew Bolt, Dan Pagendam, Piotr Szul) for providing feedback and initial testing.

# References
